﻿

//
// Менеджеры объектов конфигурации
// 

// Возвращает менеджер планов обмена
//
// Параметры:  
//
// Возвращаемое значение:
//   ПланыОбменаМенеджер
//
Функция ПланыОбменаМенеджер() Экспорт
	
	Возврат ПланыОбмена;
	
КонецФункции //КонстантыМенеджер 

// Возвращает менеджер регламентных заданий
//
// Параметры:  
//
// Возвращаемое значение:
//   РегламентныеЗаданияМенеджер
//
Функция РегламентныеЗаданияМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат РегламентныеЗадания;
	#КонецЕсли 
	
КонецФункции //КонстантыМенеджер 

// Возвращает менеджер констант
//
// Параметры:  
//
// Возвращаемое значение:
//   КонстантыМенеджер
//
Функция КонстантыМенеджер() Экспорт
	
	Возврат Константы;
	
КонецФункции //КонстантыМенеджер 

// Возвращает менежджер справочников
//
// Параметры:  
//
// Возвращаемое значение:
//   СправочникиМенеджер
//
Функция СправочникиМенеджер() Экспорт
	
	Возврат Справочники;
	
КонецФункции //СправочникиМенеджер 

// Возвращает менеджер документов
//
// Параметры:  
//
// Возвращаемое значение:
//   ДокументыМенеджер
//
Функция ДокументыМенеджер() Экспорт
	
	Возврат Документы;
	
КонецФункции //ДокументыМенеджер 

// Возвращает менеджер жерналов документов
//
// Параметры:  
//
// Возвращаемое значение:
//   ЖурналыДокументовМенеджер
//
Функция ЖурналыДокументовМенеджер() Экспорт
	
	Возврат ЖурналыДокументов;
	
КонецФункции //ЖурналыДокументовМенеджер 

// Возвращает менеджер перечислений
//
// Параметры:  
//
// Возвращаемое значение:
//   ПеречисленияМенеджер
//
Функция ПеречисленияМенеджер() Экспорт
	
	Возврат Перечисления;
	
КонецФункции //ПеречисленияМенеджер 

// Возвращает менеджер отчетов
//
// Параметры:  
//
// Возвращаемое значение:
//   ОтчетыМенеджер
//
Функция ОтчетыМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат Отчеты;
	#КонецЕсли 
	
КонецФункции //ОтчетыМенеджер 

// Возвращает менеджер обработок
//
// Параметры:  
//
// Возвращаемое значение:
//   ОбработкиМенеджер
//
Функция ОбработкиМенеджер() Экспорт
	
	Возврат Обработки;
    
КонецФункции //ОбработкиМенеджер 

// Возвращает менеджер планов видов характеристик
//
// Параметры:  
//
// Возвращаемое значение:
//   ПланыВидовХарактеристикМенеджер
//
Функция ПланыВидовХарактеристикМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат ПланыВидовХарактеристик;
	#КонецЕсли
	
КонецФункции //ПланыВидовХарактеристикМенеджер 

// Возвращает менеджер планов счетов
//
// Параметры:  
//
// Возвращаемое значение:
//   ПланыСчетовМенеджер
//
Функция ПланыСчетовМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат ПланыСчетов;
	#КонецЕсли
	
КонецФункции //ПланыСчетовМенеджер 

// Возвращает менеджер планов расчетов
//
// Параметры:  
//
// Возвращаемое значение:
//   ПланыВидовРасчетаМенеджер
//
Функция ПланыВидовРасчетаМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат ПланыВидовРасчета;
	#КонецЕсли
	
КонецФункции //ПланыВидовРасчетовМенеджер 

// Возвращает менеджер регистров сведений
//
// Параметры:  
//
// Возвращаемое значение:
//   РегистрыСведенийМенеджер
//
Функция РегистрыСведенийМенеджер() Экспорт
	
	Возврат РегистрыСведений;
    
КонецФункции //РегистрыСведенийМенеджер 

// Возвращает менеджер регистров накопления
//
// Параметры:  
//
// Возвращаемое значение:
//   РегистрыНакопленияМенеджер
//
Функция РегистрыНакопленияМенеджер() Экспорт
	
	Возврат РегистрыНакопления;
	
КонецФункции //РегистрыНакопленияМенеджер 

// Возвращает менеджер регистров бухгалтерии
//
// Параметры:  
//
// Возвращаемое значение:
//   РегистрыБухгалтерииМенеджер
//
Функция РегистрыБухгалтерииМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат РегистрыБухгалтерии;
	#КонецЕсли
	
КонецФункции //РегистрыБухгалтерииМенеджер 

// Возвращает менеджер регистров расчета
//
// Параметры:  
//
// Возвращаемое значение:
//   РегистрыРасчетаМенеджер
//
Функция РегистрыРасчетаМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат РегистрыРасчета;
	#КонецЕсли
	
КонецФункции //РегистрыРасчетаМенеджер 

// Возвращает менеджер бизнес процессов
//
// Параметры:  
//
// Возвращаемое значение:
//   БизнесПроцессыМенеджер
//
Функция БизнесПроцессыМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат БизнесПроцессы;
	#КонецЕсли
	
КонецФункции //БизнесПроцессыМенеджер 

// Возвращает менеджер задач
//
// Параметры:  
//
// Возвращаемое значение:
//   ЗадачиМенеджер
//
Функция ЗадачиМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат Задачи;
	#КонецЕсли
	
КонецФункции //ЗадачиМенеджер 

// Возвращает менеджер внешних отчетов
//
// Параметры:  
//
// Возвращаемое значение: 
// 	ВнешниеОтчетыМенеджер
//
Функция ВнешниеОтчетыМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат ВнешниеОтчеты;
	#КонецЕсли
	
КонецФункции //ВнешниеОтчетыМенеджер 

// Возвращает менеджер внешних обработок
//
// Параметры:  
//
// Возвращаемое значение: 
// 	ВнешниеОбработкиМенеджер
//
Функция ВнешниеОбработкиМенеджер() Экспорт
	
	#Если МобильноеПриложениеСервер Тогда
		Возврат Неопределено;
	#Иначе
		Возврат ВнешниеОбработки;
	#КонецЕсли
	
КонецФункции //ВнешниеОбработкиМенеджер 
