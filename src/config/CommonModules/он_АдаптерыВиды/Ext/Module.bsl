﻿// Управление типами адаптеров
// 

//
//#Область ПрограммныйИнтерфейс
//

// Регистрация типа адаптера
//
// Параметры:  
// 	Идентификатор
// 	Имя
//
// Возвращаемое значение:
// 	Справочник.уа_ТипыАдаптеров: созданный/найденный тип
// 
Функция Зарегистрировать(Идентификатор, Имя) Экспорт
	
	Возврат он_АдаптерыСервер.ВидЗарегистрировать(Идентификатор, Имя);
	
КонецФункции //Зарегистрировать 

// Возвращает тип адаптера по идентификатору
//
// Параметры:  
// 	Идентификатор:УникальныйИдентификатор
//
// Возвращаемое значение: 
// 	Справочник.уа_ТипыАдаптеров|Неопределено
//
Функция Получить(Идентификатор) Экспорт
	
	Результат	= Неопределено;
	
	ЗапросРезультат	= он_ОбъектыПрикладныеПривилегированный.ЗапросРезультатПолучить("
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	Виды.Ссылка
	|	
	|ИЗ
	|	Справочник.он_ВидыАдаптеров КАК Виды
	|	
	|ГДЕ
	|	НЕ Виды.ПометкаУдаления
	|	И Виды.Идентификатор = &Идентификатор 
	|"
	, Новый Структура("Идентификатор", Идентификатор));
	
	Если НЕ ЗапросРезультат.Пустой() Тогда
		Выборка	= ЗапросРезультат.Выбрать();
		Выборка.Следующий();
		Результат	= Выборка.Ссылка;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции //Получить 

// Возвращает список доступных видов адаптеров
//
// Параметры:  
//
// Возвращаемое значение: 
// 	СписокЗначений
// 		Значение - ссылка на описание адаптера
// 		Представление - имя вида
//
Функция Список() Экспорт
	
	Результат	= Новый СписокЗначений;
	
	ЗапросРезультат	= он_ОбъектыПрикладныеПривилегированный.ЗапросРезультатПолучить("
	|ВЫБРАТЬ
	|	Виды.Ссылка,
	|	ПРЕДСТАВЛЕНИЕ(Виды.Ссылка) КАК Представление
	|	
	|ИЗ
	|	Справочник.он_ВидыАдаптеров КАК Виды
	|	
	|ГДЕ
	|	НЕ Виды.ПометкаУдаления
	|");
	
	Если НЕ ЗапросРезультат.Пустой() Тогда
		Выборка	= ЗапросРезультат.Выбрать();
		Пока Выборка.Следующий() Цикл
			Результат.Добавить(Выборка.Ссылка, Выборка.Представление);
		КонецЦикла;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции //Список 

//#КонецОбласти ПрограммныйИнтерфейс

