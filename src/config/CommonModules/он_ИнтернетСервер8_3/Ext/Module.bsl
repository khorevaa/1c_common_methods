﻿//
// Конструкторы приложения 8.3
// 

// Возвращает подключение к определению веб сервиса
//
// Параметры: 
// 	СерверАдрес
// 	ПубликацияИмя
// 	Пользователь
// 	Пароль
// 	ИнтернетПрокси
// 	Таймаут
// 	ЗащищенноеСоединение
// 	АутенфикацияОС
//
// Возвращаемое значение:
//   WSОпределение|Неопределено
//
Функция ВебСервисОпределение(СерверАдрес
	, ПубликацияИмя
	, Пользователь = Неопределено
	, Пароль = Неопределено
	, ИнтернетПрокси = Неопределено
	, Таймаут = Неопределено
	, ЗащищенноеСоединение = Неопределено) Экспорт
	
	Результат	= Неопределено;
	
	#Если НЕ (ТонкийКлиент ИЛИ ВебКлиент ИЛИ МобильноеПриложениеКлиент) Тогда
		Попытка
			Результат	= Новый WSОпределения(?(Прав(СерверАдрес, 1) = "/", СерверАдрес, СерверАдрес + "/") + "ws/" + ПубликацияИмя + ".1cws?wsdl"
			, Пользователь
			, Пароль
			, ИнтернетПрокси
			, Таймаут
			, ЗащищенноеСоединение);
		Исключение
		КонецПопытки;
	#КонецЕсли
	
	Возврат Результат;
	
КонецФункции //ВебСервисОпределение 

// Возвращает прокси к веб сервису
//
// Параметры: 
// 	Определение
// 	ПространствоИмен
// 	СервисИмя
// 	ТочкаПодключенияИмя
// 	ИнтернетПрокси
// 	Таймаут
// 	ЗащищенноеСоединение
// 	АутенфикацияОС
//
// Возвращаемое значение:
//   WSПрокси|Неопределено
//
Функция ВебСервис(Определение
	, ПространствоИмен
	, СервисИмя
	, ТочкаПодключенияИмя = Неопределено
	, ИнтернетПрокси = Неопределено
	, Таймаут = Неопределено
	, ЗащищенноеСоединение = Неопределено) Экспорт
	
	Результат	= Неопределено;
	
	#Если НЕ (ТонкийКлиент ИЛИ ВебКлиент ИЛИ МобильноеПриложениеКлиент) Тогда
		Результат	= Новый WSПрокси(Определение
		, ПространствоИмен
		, СервисИмя
		, ?(ТочкаПодключенияИмя = Неопределено, СервисИмя + "Soap", ТочкаПодключенияИмя)
		, ИнтернетПрокси
		, Таймаут
		, ЗащищенноеСоединение);
	#КонецЕсли
	
	Возврат Результат;
	
КонецФункции //ВебСервисПрокси 

