﻿//
// Программное создание схемы компоновки данных
// 
// Использование
// Пример для компоновщика настроек размещенного на форме для организации отбора
// Имя реквизита компоновщика предполагаем СписокНастройка
// 
// Описание	= он_СхемаКомпоновкиДанных.ОписаниеСоздать("
// |ВЫБРАТЬ ПЕРВЫЕ 1 РАЗРЕШЕННЫЕ
// |	Организации.Ссылка
// |
// |ИЗ
// |	Справочник.Организации КАК Организации
// |
// |ГДЕ
// |	НЕ Организации.ПометкаУдаления");
// он_СхемаКомпоновки.ГруппировкаДобавить("Ссылка");
// 
// // Получаем схему компоновки
// Схема	= он_СхемаКомпоновкиДанных.Создать(Описание);
// // Помещаем схему во временное хранилище
// Адрес	= ПоместитьВоВременноеХранилище(Схема);
// // Инициализируем компоновщик
// СписокНастройка.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(Адрес));
// // И загружаем настройки
// СписокНастройка.ЗагрузитьНастройки(Схема.НастройкиПоУмолчанию);
// 

// Возвращает структуру описания схемы компоновки
//
// Параметры:  
//
// Возвращаемое значение: 
// 	Структура
//
Функция ОписаниеСоздать(ЗапросТекст = "") Экспорт
	
	Возврат Новый Структура("ЗапросТекст, Группировки, Поля, Ресурсы, Параметры"
	, ЗапросТекст
	, он_Коллекции.ТаблицаЗначенийСоздать("Имя")
	, он_Коллекции.ТаблицаЗначенийСоздать("Группировка, Имя, Представление")
	, он_Коллекции.ТаблицаЗначенийСоздать("Имя, Представление, Выражение, ФорматСтрока")
	, он_Коллекции.ТаблицаЗначенийСоздать("Имя, Представление, Значение"));
	
КонецФункции //ОписаниеСоздать 

// Устанавливает текст запроса описания. Вовращает значение на момент до выполнения метода
//
// Параметры:  
// 	Описание
// 	ЗапросТекст
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЗапросТекст(Описание, ЗапросТекст = Неопределено) Экспорт
	
	Результат	= Описание.ЗапросТекст;
	
	Если ЗапросТекст <> Неопределено Тогда
		Описание.ЗапросТекст	= ЗапросТекст;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции //ЗапросТекст 

// Возвращает группировки описания СКД
//
// Параметры:  
// 	Описание
//
// Возвращаемое значение: 
// 	ТаблицаЗначений
//
Функция ОписаниеГруппировки(Описание)
	
	Возврат Описание.Группировки;
	
КонецФункции //ОписаниеГруппировки 

// Возвращает таблицу полей группировок
//
// Параметры:  
// 	Описание
//
// Возвращаемое значение: 
// 	ТаблицаЗначений
//
Функция ОписаниеПоля(Описание)
	
	Возврат Описание.Поля;
	
КонецФункции //ОписаниеПоля  

// Возвращает таблицу ресурсов СКД
//
// Параметры:  
// 	Описание
//
// Возвращаемое значение: 
// 	ТаблицаЗначений
//
Функция ОписаниеРесурсы(Описание) 
	
	Возврат Описание.Ресурсы;
	
КонецФункции //ОписаниеРесурсы 

// Возвращает таблицу параметров СКД
//
// Параметры:  
// 	Описание
//
// Возвращаемое значение: 
// 	ТаблицаЗначений
//
Функция ОписаниеПараметры(Описание)
	
	Возврат Описание.Параметры;
	
КонецФункции //ОписаниеПараметры 

// Добавляет группировку 
//
// Параметры:  
// 	Описание
// 	Имя
// 	Представление
//
Процедура ГруппировкаДобавить(Описание, Имя, Представление = Неопределено) Экспорт
	
	ГруппировкиСтрока	= ОписаниеГруппировки(Описание).Добавить();
	ГруппировкиСтрока.Имя	= Имя;
	
	// Сразу добавляем поле группировки
	ПолеДобавить(Описание, Имя, Имя, Представление);
	
КонецПроцедуры //ГруппировкаДобавить 

// Добавляет поле группировки 
//
// Параметры:  
// 	Описание
// 	Группировка
// 	Имя
// 	Представление
//
Процедура ПолеДобавить(Описание, Группировка, Имя, Представление = Неопределено) Экспорт
	
	ПоляСтрока	= ОписаниеПоля(Описание).Добавить();
	ПоляСтрока.Группировка		= Группировка;
	ПоляСтрока.Имя				= Имя;
	ПоляСтрока.Представление	= Представление;
	
КонецПроцедуры //ПолеДобавить 

// Добавлет ресурс
//
// Параметры:  
// 	Описание
// 	Имя
// 	Выражение:Строка - функция запроса СУММА(Ресурс)|МИНИМУМ(Ресурс)|...
// 	Представление
// 	ФорматСтрока
//
Процедура РесурсДобавить(Описание, Имя, Выражение, Представление = Неопределено, ФорматСтрока = "ЧЦ=18; ЧДЦ=2; ЧН=") Экспорт
	
	РесурсыСтрока	= ОписаниеРесурсы(Описание).Добавить();
	РесурсыСтрока.Имя			= Имя;
	РесурсыСтрока.Выражение		= Выражение;
	РесурсыСтрока.Представление	= Представление;
	РесурсыСтрока.ФорматСтрока	= ФорматСтрока;
	
КонецПроцедуры //РесурсДобавить 

// Добавляет параметр СКД
//
// Параметры:  
// 	Описание
// 	Имя
// 	Значение
// 	Представление
//
Процедура ПараметрДобавить(Описание, Имя, Значение, Представление = Неопределено) Экспорт
	
	ПараметрыСтрока	= ОписаниеПараметры(Описание).Добавить();
	ПараметрыСтрока.Имя				= Имя;
	ПараметрыСтрока.Значение		= Значение;
	ПараметрыСтрока.Представление	= Представление;
	
КонецПроцедуры //ПараметрДобавить 

// Добавляет доступные поля в набор данных (закладка Наборы данных)
//
// Параметры:  
// 	Набор
// 	ПоляТаблица
//
Процедура НаборПоляДобавить(Набор, ПоляТаблица) 
	
	// Устанавливаем доступные поля набора данных
	Для каждого ПолеСтрока Из ПоляТаблица Цикл
		НаборПоле				= Набор.Поля.Добавить(Тип("ПолеНабораДанныхСхемыКомпоновкиДанных"));
		Если ЗначениеЗаполнено(ПолеСтрока.Представление) Тогда
			НаборПоле.Заголовок		= ПолеСтрока.Представление;
		КонецЕсли;
		НаборПоле.Поле			= ПолеСтрока.Имя;
		НаборПоле.ПутьКДанным	= ПолеСтрока.Имя;
		ФорматСтрока	= он_ОбъектыПрикладные.СвойствоПолучить(ПолеСтрока, "ФорматСтрока");
		Если НЕ ПустаяСтрока(ФорматСтрока) Тогда
			ФорматнаяСтрока	= НаборПоле.Оформление.Элементы.Найти("Формат");
			ФорматнаяСтрока.Значение		= ФорматСтрока;
			ФорматнаяСтрока.Использование	= Истина;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры //НаборПоляДобавить 

// Добавление ресурсов в схему (закладка Ресурсы)
//
// Параметры:  
// 	Схема
// 	РесурсыТаблица
//
Процедура СхемаРесурсыДобавить(Схема, РесурсыТаблица)
	
	Для каждого РесурсСтрока Из РесурсыТаблица Цикл
		РесурсПоле	= Схема.ПоляИтога.Добавить();
		РесурсПоле.Выражение	= СокрЛП(РесурсСтрока.Выражение);
		РесурсПоле.ПутьКДанным	= РесурсСтрока.Имя;
	КонецЦикла;
	
КонецПроцедуры //СхемаРесурсыДобавить 

// Устанавливает значение параметра вывода схемы компоновки данных (закладка Настройки)
//
// Параметры:  
// 	Настройки
// 	Имя
// 	Значение
//
Процедура НастройкиВыводПараметрУстановить(Настройки, Имя, Значение)
	
	Параметр	= Настройки.ПараметрыВывода.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных(Имя));
	Если Параметр <> Неопределено Тогда
		Параметр.Использование	= Истина;
		Параметр.Значение		= Значение;
	КонецЕсли;
	
КонецПроцедуры //НастройкиВыводПараметрУстановить 

// Добавляет в схему группировки (закладка Настройки)
//
// Параметры:  
// 	Схема
// 	ГруппировкиТаблица
//
Процедура НастройкиГруппировкиДобавить(Настройки, ГруппировкиТаблица, ПоляТаблица)
	
	// Группировки отчета
	ГруппировкаРодитель	= Настройки;
	Для каждого ГруппировкаСтрока Из ГруппировкиТаблица Цикл
		КомпонопкаГруппировка	= ГруппировкаРодитель.Структура.Добавить(Тип("ГруппировкаКомпоновкиДанных"));
		КомпонопкаГруппировка.Имя			= ГруппировкаСтрока.Имя;
		КомпонопкаГруппировка.Использование	= Истина;
		
		ГруппировкаРодитель		= КомпонопкаГруппировка;
		
		// Добавляем уопрядочивание по группировке
		ПолеСортировки	= КомпонопкаГруппировка.Порядок.Элементы.Добавить(Тип("АвтоЭлементПорядкаКомпоновкиДанных"));
		ПолеСортировки.Использование	= Истина;
		
		// Поля группировки
		Для каждого Поле Из ПоляТаблица.НайтиСтроки(Новый Структура("Группировка", ГруппировкаСтрока.Имя)) Цикл
			ГруппировкаПоле	= КомпонопкаГруппировка.ПоляГруппировки.Элементы.Добавить(Тип("ПолеГруппировкиКомпоновкиДанных"));
			ГруппировкаПоле.Использование	= Истина;
			ГруппировкаПоле.Поле			= Новый ПолеКомпоновкиДанных(Поле.Имя);
			ГруппировкаПоле.ТипГруппировки	= ТипГруппировкиКомпоновкиДанных.Элементы;
			ГруппировкаПоле.ТипДополнения	= ТипДополненияПериодаКомпоновкиДанных.БезДополнения;
			
			ГруппировкаАвтоПоле	= КомпонопкаГруппировка.Выбор.Элементы.Добавить(Тип("АвтоВыбранноеПолеКомпоновкиДанных"));
			ГруппировкаАвтоПоле.Использование	= Истина;
		КонецЦикла;
	КонецЦикла;
	
КонецПроцедуры //НастройкиГруппировкиДобавить 

// Устанавливает параметры настроек (закладка Настройки)
//
// Параметры:  
// 	Настройки
// 	ПараметрыТаблица
//
Процедура НастройкиПараметрыУстановить(Настройки, ПараметрыТаблица)
	
	// Объявляем параметры схемы для вывода в отчет
	Для каждого ПараметрыСтрока Из ПараметрыТаблица Цикл
		Параметр	= Настройки.ПараметрыДанных.Элементы.Добавить();
		Параметр.Значение			= ПараметрыСтрока.Значение;
		Параметр.Использование		= Истина;
		Параметр.Параметр			= Новый ПараметрКомпоновкиДанных(ПараметрыСтрока.Имя);
		Параметр.РежимОтображения	= РежимОтображенияЭлементаНастройкиКомпоновкиДанных.Авто;
	КонецЦикла;
	
КонецПроцедуры //НастройкиПараметрыУстановить 

// Устанавливает параметры схемы компоновки (закладка Параметры)
//
// Параметры:  
// 	Схема
// 	ПараметрыТаблица
//
Процедура СхемаПараметрыУстановить(Схема, ПараметрыТаблица)
	
	Для каждого ПараметрыСтрока Из ПараметрыТаблица Цикл
		Параметр	= Схема.Параметры.Добавить();
		Параметр.Имя						= ПараметрыСтрока.Имя;
		Параметр.Заголовок					= ПараметрыСтрока.Представление;
		Параметр.Значение					= ПараметрыСтрока.Значение;
		Параметр.ОграничениеИспользования	= Ложь;
		Параметр.Использование				= ИспользованиеПараметраКомпоновкиДанных.Всегда;
	КонецЦикла;
	
КонецПроцедуры //СхемаПараметрыУстановить 

// Возвращает созданную по описанию схему компоновки данных
//
// Параметры:  
// 	Описание
// 	Адрес
// 	Заголовок
//
// Возвращаемое значение: 
// 	СхемаКомпоновкиДанных
//
Функция Создать(Описание, Заголовок = Неопределено, ПараметрыВыводить = Истина) Экспорт
	
	Результат	= Неопределено;
	
	#Если НЕ МобильноеПриложениеСервер Тогда 
		
		Результат	= Новый СхемаКомпоновкиДанных;
		
		// Объявляет источник данных для схемы
		Источник = Результат.ИсточникиДанных.Добавить();
		Источник.Имя				= он_Система.ПолучитьУникальноеИмя( , , Истина);
		Источник.СтрокаСоединения	= "";
		Источник.ТипИсточникаДанных	= "Local"; 
		
		ДанныеНабор = Результат.НаборыДанных.Добавить(Тип("НаборДанныхЗапросСхемыКомпоновкиДанных"));
		ДанныеНабор.Имя 			= "НаборДанных";
		ДанныеНабор.ИсточникДанных	= Источник.Имя;
		ДанныеНабор.Запрос			= ЗапросТекст(Описание);
		
		// Добавим авто поля
		ДанныеНабор.АвтоЗаполнениеДоступныхПолей = Истина;
		
		// Добаляем доступные поля набора данных
		НаборПоляДобавить(ДанныеНабор, ОписаниеПоля(Описание));
		НаборПоляДобавить(ДанныеНабор, ОписаниеРесурсы(Описание));
		
		// Ресурсы
		СхемаРесурсыДобавить(Результат, ОписаниеРесурсы(Описание));
		
		// Настройки вывода схемы
		Настройки	= Результат.НастройкиПоУмолчанию;
		Если Заголовок <> Неопределено Тогда
			НастройкиВыводПараметрУстановить(Настройки, "ВыводитьЗаголовок", ТипВыводаТекстаКомпоновкиДанных.Выводить);
			НастройкиВыводПараметрУстановить(Настройки, "Заголовок", Заголовок);
		КонецЕсли;
		Если ПараметрыВыводить = Истина Тогда
			НастройкиВыводПараметрУстановить(Настройки, "ВыводитьПараметрыДанных", ТипВыводаТекстаКомпоновкиДанных.Выводить);
		КонецЕсли;
		// Выводимые группировки
		НастройкиГруппировкиДобавить(Настройки, ОписаниеГруппировки(Описание), ОписаниеПоля(Описание));
		// Параметры  настройки
		НастройкиПараметрыУстановить(Настройки, ОписаниеПараметры(Описание));
		
		// Параметры  самой схемы данных
		СхемаПараметрыУстановить(Результат, ОписаниеПараметры(Описание));
	#КонецЕсли 
	
	Возврат Результат;
	
КонецФункции //Создать 